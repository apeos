# Copyright 2016-2017 Espressif Systems (Shanghai) PTE LTD
# Copyright 2018 apeos contributors
#
# Main bootloader Makefile.
#
# This is basically the same as a component makefile, but in the case of the bootloader
# we pull in bootloader-specific linker arguments.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LINKER_SCRIPTS := \
	esp32.bootloader.ld \
	$(IDF_PATH)/components/esp32/ld/esp32.rom.ld \
	$(IDF_PATH)/components/esp32/ld/esp32.rom.spiram_incompatible_fns.ld \
	$(IDF_PATH)/components/esp32/ld/esp32.peripherals.ld \
	esp32.bootloader.rom.ld

ifndef CONFIG_SPI_FLASH_ROM_DRIVER_PATCH
LINKER_SCRIPTS += $(IDF_PATH)/components/esp32/ld/esp32.rom.spiflash.ld
endif

COMPONENT_ADD_LDFLAGS += -L $(COMPONENT_PATH) $(addprefix -T ,$(LINKER_SCRIPTS))

COMPONENT_ADD_LINKER_DEPS := $(LINKER_SCRIPTS)

COMPONENT_SRCDIRS += apeos
