# Copyright 2018 apeos contributors
#
# apeos partial GNU makefile, build binary program from C sources
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# variables that have to be set before evaulating this program:
#
# BIN - name of the resulting binary program
# SRCS - whitespace separated list of source filenames in /src/,
#     without /src/ prefix.

TOP_DIR := $(shell pwd)
#$(warning TOP_DIR=$(TOP_DIR))
BUILD_DIR_BASE := $(TOP_DIR)/build
#$(warning BUILD_DIR_BASE=$(BUILD_DIR_BASE))
BUILD_PREFIX := $(BUILD_DIR_BASE)/$(BIN)-
#$(warning BUILD_PREFIX=$(BUILD_PREFIX))
SRC_DIR := $(TOP_DIR)/src

BIN_PATH := $(BUILD_DIR_BASE)/$(BIN)

# by default, create the resulting binary file
.PHONY: default
default: $(BIN_PATH)
	@file $(BIN_PATH)

#$(warning SRC_DIR=$(SRC_DIR))
SRCS_PREFIXED := $(addprefix $(SRC_DIR)/,$(SRCS))
#$(warning SRCS_PREFIXED="$(SRCS_PREFIXED)")
OBJS := $(addprefix $(BUILD_PREFIX),$(patsubst %.c,%.o,$(SRCS)))
#$(warning OBJS="$(OBJS)")

BUILT_FILES := $(OBJS) $(BIN)

$(BUILT_FILES): program.mk

CPPFLAGS := -I include

CFLAGS := -pipe
LDFLAGS :=

ifeq ($(CFG),debug)
CFLAGS += -O0
CFLAGS += -g
LDFLAGS += -g
endif
ifeq ($(CFG),size)
CFLAGS := -Os
endif

CFLAGS += -static
LDFLAGS += -static

# gcc
CC := $(GCC_PREFIX)gcc
LD := $(GCC_PREFIX)gcc

ifneq ($(V),1)
CC := @$(CC)
LD := @$(LD)
endif

$(BUILD_DIR_BASE):
	@mkdir -v $@

$(BUILT_FILES): | $(BUILD_DIR_BASE)

$(BUILD_PREFIX)%.o: $(SRC_DIR)/%.c
	@echo [CC] $(shell basename $<)
	$(CC) -c $< $(CPPFLAGS) $(CFLAGS) -o $@

$(BIN_PATH): $(OBJS)
	@echo [LD] $(BIN)
	$(LD) $(OBJS) $(LDFLAGS) -o $@

.PHONY: clean
clean:
	@rm -vf $(BUILT_FILES)
