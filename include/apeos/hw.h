/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*
 * Copyright 2018 apeos contributors
 *
 * apeos hardware interface functions
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HW_H__A9C789E7_16C2_4F4C_A982_90F8483D0CBF__INCLUDED
#define HW_H__A9C789E7_16C2_4F4C_A982_90F8483D0CBF__INCLUDED

void hw_init(void);
void hw_uninit(void);
void hw_info(void);

#endif /* #ifndef HW_H__A9C789E7_16C2_4F4C_A982_90F8483D0CBF__INCLUDED */
