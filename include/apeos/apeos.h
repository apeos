/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*
 * Copyright 2018 apeos contributors
 *
 * apeos common definitions
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef APEOS_H__6253DA2F_EC5A_4D19_82AE_BA63F09A32D7__INCLUDED
#define APEOS_H__6253DA2F_EC5A_4D19_82AE_BA63F09A32D7__INCLUDED

/* c runtime */
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/* posix */
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#endif /* #ifndef APEOS_H__6253DA2F_EC5A_4D19_82AE_BA63F09A32D7__INCLUDED */
