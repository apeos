# Copyright 2018 apeos contributors
#
# apeos GNU makefile, build esp apeos via esp-idf
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

PROJECT_NAME := apeos

# customize esp-idf components
COMPONENTS := bootloader # modified esp-idf bootloader, Apache License, Version 2.0
COMPONENTS += esptool_py # modified esp-idf esptool for installing apeos on esp-32 flash, GPLv2+

# apeos is being built
EXTRA_CPPFLAGS := -DAPEOS

# apeos is being built through esp-idf based build system
EXTRA_CPPFLAGS += -DESP_IDF_APEOS_BUILD

# Don't wear flash storage when building on systems
# where tmp files are stored on flash.
# Also probably faster unless /tmp is ramfs.
EXTRA_CFLAGS := -pipe

$(if $(V),$(info including make/project.mk...))
include make/project.mk
