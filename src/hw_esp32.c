/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*
 * Copyright 2018 apeos contributors
 *
 * apeos Espressif esp-32 hardware interface functions
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <apeos/apeos.h>
#include <apeos/hw.h>
#include <rom/ets_sys.h>         /* ets_printf */
#include <rom/spi_flash.h>       /* g_rom_flashchip */
#include <soc/rtc_cntl_reg.h>    /* RTC_CNTL_WDTCONFIG0_REG */
#include <soc/timer_group_reg.h> /* TIMG_WDTCONFIG0_REG */
#include <soc/cpu.h>             /* esp_cpu_stall() */
#include <soc/efuse_reg.h>       /* EFUSE_BLK0_RDATA3_REG */

static void esp32_disable_watchdog(void)
{
  REG_CLR_BIT(RTC_CNTL_WDTCONFIG0_REG, RTC_CNTL_WDT_FLASHBOOT_MOD_EN);
  REG_CLR_BIT(TIMG_WDTCONFIG0_REG(0), TIMG_WDT_FLASHBOOT_MOD_EN);
}

void hw_init(void)
{
  ets_printf("apeos booting\n");
  esp32_disable_watchdog();
}

void hw_uninit(void)
{
  esp_cpu_stall(0);
}

static
bool
esp32_package_get_info(
  uint32_t efuse_blk0_rdata3_reg,
  bool * embedded_flash_ptr,
  const char ** package_description_ptr_ptr)
{
  const char * package_description_ptr;
  bool embedded_flash;

  switch ((efuse_blk0_rdata3_reg & EFUSE_RD_CHIP_VER_PKG_M)
          >> EFUSE_RD_CHIP_VER_PKG_S)
  {
  case EFUSE_RD_CHIP_VER_PKG_ESP32D2WDQ5:
    package_description_ptr = "D2WDQ5";
    embedded_flash = true;
    break;
  case EFUSE_RD_CHIP_VER_PKG_ESP32PICOD2:
    package_description_ptr = "PICOD2";
    embedded_flash = true;
    break;
  case EFUSE_RD_CHIP_VER_PKG_ESP32PICOD4:
    package_description_ptr = "PICOD4";
    embedded_flash = true;
    break;
  default:
    package_description_ptr = NULL;
  }

  if (package_description_ptr == NULL)  /* unknown chip package */
  {
    *package_description_ptr_ptr = "unknown";
    return false;
  }

  *package_description_ptr_ptr = package_description_ptr;
  *embedded_flash_ptr = embedded_flash;
  return true;
}

/* Print chip information */
void hw_info(void)
{
  uint32_t reg;
  bool known_package;
  bool embedded_flash;
  const char * package_description_ptr;
  bool bluetooth;
  uint32_t spiflash_chip_size;

  reg = REG_READ(EFUSE_BLK0_RDATA3_REG);

  known_package = esp32_package_get_info(
    reg,
    &embedded_flash,
    &package_description_ptr);

  /* bluetooth is enabled if not found disabled in efuse */
  bluetooth = (reg & EFUSE_RD_CHIP_VER_DIS_BT_M) == 0;

  /* size of SPI flash chip, in bytes */
  spiflash_chip_size = g_rom_flashchip.chip_size;

  ets_printf(
    "ESP32 SoC");

  if (known_package)
  {
    ets_printf(
      ", %s package",
      package_description_ptr);
  }

  ets_printf(
    ", silicon revision %d", ((reg & EFUSE_RD_CHIP_VER_REV1_M) != 0) ? 1 : 0);

  ets_printf(
    ", %d CPU cores",
    ((reg & EFUSE_RD_CHIP_VER_DIS_APP_CPU_M) == 0) ? 2 : 1);

  ets_printf(
    ", bluetooth %s",
    bluetooth ? "enabled" : "disabled");

  ets_printf(
    ", %u byte %sflash\n",
    (unsigned int)spiflash_chip_size,
    known_package ? (embedded_flash ? "embedded " : "external ") : "");
}
