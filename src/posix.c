/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*
 * Copyright 2018 apeos contributors
 *
 * host apeos on posix system
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined(ESP_IDF_APEOS_BUILD) /* enumerating files in esp-idf build
                                     system is harder than this */

#include <apeos/apeos.h>
#include <apeos/hw.h>
#include <apeos/boot.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/utsname.h>

void hw_init(void)
{
  printf("apeos booting\n");
}

void hw_uninit(void)
{
  exit(0);
}

void hw_info(void)
{
  struct utsname info;

  printf("POSIX");
  if (uname(&info) == 0)
  {
    printf(
      " %s [%s %s %s] %s",
      info.machine,
      info.sysname,
      info.release,
      info.version,
      info.nodename);
  }
  printf("\n");
}

int main(void)
{
  apeos_boot();
}

#endif
